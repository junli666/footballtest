﻿using System;

namespace FootBall.Core
{
    public class Validator
    {
        public static bool ValidateLine(string line)
        {
            var lineToken = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (lineToken.Length < 10) return false;
            
            if (int.TryParse(lineToken[2], out int spaceHolder)
                 && int.TryParse(lineToken[3], out spaceHolder)
                 && int.TryParse(lineToken[4], out spaceHolder)
                 && int.TryParse(lineToken[5], out spaceHolder)
                 && int.TryParse(lineToken[6], out spaceHolder)
                 && int.TryParse(lineToken[8], out spaceHolder)
                 && int.TryParse(lineToken[9], out spaceHolder))
            {
                return true;
            }

            return false;
        }
    }
}

﻿using FootBall.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace FootBall.Core
{
    public class SearchClub
    {
        private readonly IEnumerable<Club> _clubs;

        public SearchClub(List<Club> clubs)
        {
            _clubs = clubs;
        }
        public SearchClub(string[] lines)
        {
            var validLines = lines
                .Where(Validator.ValidateLine);

            _clubs = Mapper.LinesToClubList(validLines);
        }

        public Club GetSmallestGoalDifference()
        {
            if (_clubs == null) return null;
            return _clubs.OrderBy(_ => _.GoalDifference).FirstOrDefault();
        }
    }
}

﻿using FootBall.Core.Model;
using System;
using System.Collections.Generic;

namespace FootBall.Core
{
    public class Mapper
    {
        public static IEnumerable<Club> LinesToClubList(IEnumerable<string> lines)
        {
            foreach (var line in lines)
            {
                var columns = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                yield return new Club
                {
                    ClubName = columns[1],
                    Play = int.Parse(columns[2]),
                    Win = int.Parse(columns[3]),
                    Lose = int.Parse(columns[4]),
                    Draw = int.Parse(columns[5]),
                    GoalFor = int.Parse(columns[6]),
                    GoalAgainst = int.Parse(columns[8]),
                    Points = int.Parse(columns[9])
                };
            }
        }
    }
}

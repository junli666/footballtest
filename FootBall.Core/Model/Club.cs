﻿using System;

namespace FootBall.Core.Model
{
    public class Club
    {
        public string ClubName { get; set; }
        public int Play { get; set; }
        public int Win { get; set; }
        public int Lose { get; set; }
        public int Draw { get; set; }
        public int GoalFor { get; set; }
        public int GoalAgainst { get; set; }
        public int Points { get; set; }

        public int GoalDifference => Math.Abs(GoalFor - GoalAgainst);

        public override string ToString()
        {
            return $"Club Name:{ClubName,-15}Play:{Play,-5}Win:{Win,-5}Lose:{Lose,-5}Draw:{Draw,-5}Goal For:{GoalFor,-5}Goal Against:{GoalAgainst,-5}Points:{Points,-5}";
        }
    }
}
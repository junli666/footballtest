﻿using FootBall.Core;
using FootBall.Core.Model;
using System.IO;

namespace FootBall.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = ProcessClubs("footBall.dat");

            System.Console.WriteLine(result);
            System.Console.ReadLine();
        }

        private static Club ProcessClubs(string path)
        {
            var fileContent = File.ReadAllLines(path);

            var service = new SearchClub(fileContent);

            return service.GetSmallestGoalDifference();
        }
    }
}
